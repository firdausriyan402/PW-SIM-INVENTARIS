<?php

use App\Http\Controllers\Admin\Data\BarangController;
use App\Http\Controllers\Admin\Data\GedungController;
use App\Http\Controllers\Admin\Data\RuanganController;
use App\Http\Controllers\Admin\Data\TanahController;
use App\Http\Controllers\Admin\Peminjaman\PeminjamanBarangController;
use App\Http\Controllers\Admin\Peminjaman\PeminjamanGedungController;
use App\Http\Controllers\Admin\Peminjaman\PeminjamanRuanganController;
use App\Http\Controllers\Admin\Peminjaman\PeminjamanTanahController;
use App\Http\Controllers\Admin\PenggunaController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

// Route Auth
Route::get('/', [AuthController::class, 'index'])->name('auth.index');
Route::post('/', [AuthController::class, 'authanticate'])->name('auth.authen');

Route::middleware(['guest'])->group(function () {
    Route::get('/register', [AuthController::class, 'register'])->name('auth.register');
    Route::post('/register', [AuthController::class, 'store'])->name('auth.store');  
});


Route::middleware(['isLogin', 'CheckRole:Admin,User,Guru'])->group(function () {
    // Route Dashboard
    Route::get('admin-dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('admin-dashboard/pengguna/My-Profile', [PenggunaController::class, 'myProfile'])->name('pengguna.myProfile');
    Route::put('admin-dashboard/pengguna/My-Profile', [PenggunaController::class, 'update'])->name('my-profile.update');
    Route::post('/logout', [AuthController::class, 'logout'])->name('auth.logout');
    Route::get('/reset', [AuthController::class, 'reset'])->name('auth.reset');
    
    // Route Peminjaman Barang
    Route::get('admin-dashboard/Peminjaman-Barang', [PeminjamanBarangController::class, 'index'])->name('peminjamanBarang.index');
    Route::get('admin-dashboard/Peminjaman-Barang/add', [PeminjamanBarangController::class, 'create'])->name('peminjamanBarang.add');

    Route::get('admin-dashboard/Peminjaman-Ruangan', [PeminjamanRuanganController::class, 'index'])->name('peminjamanRuangan.index');
    Route::get('admin-dashboard/Peminjaman-Ruangan/add', [PeminjamanRuanganController::class, 'create'])->name('peminjamanRuangan.add');

    Route::get('admin-dashboard/Peminjaman-Gedung', [PeminjamanGedungController::class, 'index'])->name('peminjamanGedung.index');
    Route::get('admin-dashboard/Peminjaman-Gedung/add', [PeminjamanGedungController::class, 'create'])->name('peminjamanGedung.add');

    Route::get('admin-dashboard/Peminjaman-Tanah', [PeminjamanTanahController::class, 'index'])->name('peminjamanTanah.index');
});


// Route khsus Admin
Route::middleware(['isLogin', 'CheckRole:Admin'])->group(function () {
    Route::get('admin-dashboard/pengguna', [PenggunaController::class, 'index'])->name('pengguna.index');
    Route::get('admin-dashboard/pengguna/edit', [PenggunaController::class, 'edit'])->name('pengguna.edit');
    Route::get('admin-dashboard/pengguna/add', [PenggunaController::class, 'create'])->name('pengguna.add');
    Route::post('admin-dashboard/pengguna/add', [AuthController::class, 'store'])->name('pengguna.store');

    // Route Gedung
    Route::get('admin-dashboard/Gedung', [GedungController::class, 'index'])->name('gedung.index');
    Route::get('admin-dashboard/Gedung/download-pdf', [GedungController::class, 'pdf'])->name('gedung.pdf');
    Route::get('admin-dashboard/Gedung/add', [GedungController::class, 'create'])->name('gedung.create');
    Route::post('admin-dashboard/Gedung/add', [GedungController::class, 'store'])->name('gedung.store');
    Route::get('admin-dashboard/Gedung/{id}/edit', [GedungController::class, 'edit'])->name('gedung.edit');
    Route::put('admin-dashboard/Gedung/{id}/edit', [GedungController::class, 'update'])->name('gedung.update');
    Route::delete('admin-dashboard/Gedung/{id}/delete', [GedungController::class, 'destroy'])->name('gedung.destroy');
    Route::get('admin-dashboard/Gedung/export', [GedungController::class, 'export'])->name('gedung.export');

    // Route Ruangan
    Route::get('admin-dashboard/Ruangan', [RuanganController::class, 'index'])->name('ruangan.index');
    Route::get('admin-dashboard/Ruangan/download-pdf', [RuanganController::class, 'pdf'])->name('ruangan.pdf');
    Route::get('admin-dashboard/Ruangan/add', [RuanganController::class, 'create'])->name('ruangan.add');
    Route::post('admin-dashboard/Ruangan/add', [RuanganController::class, 'store'])->name('ruangan.store');
    Route::get('admin-dashboard/Ruangan/{id}/edit', [RuanganController::class, 'edit'])->name('ruangan.edit');
    Route::put('admin-dashboard/Ruangan/{id}/edit', [RuanganController::class, 'update'])->name('ruangan.update');
    Route::delete('admin-dashboard/Ruangan/{id}/delete', [RuanganController::class, 'destroy'])->name('ruangan.destroy');
    Route::get('admin-dashboard/Ruangan/export', [RuanganController::class, 'export'])->name('ruangan.export');

    // Route Tanah
    Route::get('admin-dashboard/Tanah', [TanahController::class, 'index'])->name('tanah.index');
    Route::get('admin-dashboard/Tanah/download-pdf', [TanahController::class, 'pdf'])->name('tanah.pdf');
    Route::get('admin-dashboard/Tanah/add', [TanahController::class, 'create'])->name('tanah.add');
    Route::post('admin-dashboard/Tanah/add', [TanahController::class, 'store'])->name('tanah.store');
    Route::get('admin-dashboard/Tanah/{id}/edit', [TanahController::class, 'edit'])->name('tanah.edit');
    Route::put('admin-dashboard/Tanah/{id}/update', [TanahController::class, 'update'])->name('tanah.update');
    Route::delete('admin-dashboard/Tanah/{id}/delete', [TanahController::class, 'destroy'])->name('tanah.destroy');
    Route::get('admin-dashboard/Tanah/export', [TanahController::class, 'export'])->name('tanah.export');

    // Route Barang
    Route::get('admin-dashboard/Barang', [BarangController::class, 'index'])->name('barang.index');
    Route::get('admin-dashboard/Barang/download-pdf', [BarangController::class, 'pdf'])->name('barang.pdf');
    Route::get('admin-dashboard/Barang/add', [BarangController::class, 'create'])->name('barang.add');
    Route::post('admin-dashboard/Barang/add', [BarangController::class, 'store'])->name('barang.store');
    Route::get('admin-dashboard/Barang/{id}/edit', [BarangController::class, 'edit'])->name('barang.edit');
    Route::put('admin-dashboard/Barang/{id}/edit', [BarangController::class, 'update'])->name('barang.update');
    Route::delete('admin-dashboard/Barang/{id}/delete', [BarangController::class, 'destroy'])->name('barang.destroy');
    Route::get('admin-dashboard/Barang/export', [BarangController::class, 'export'])->name('barang.export');
});
