<div class="header">
    <p>Header Dokumen Resmi</p>
</div>

<table>
    <thead>
        <tr>
            <th class="w-10px pe-2">#</th>
            <th class="min-w-200px">Nama Gedung</th>
            <th class="min-w-200px">ID Gedung</th>
            <th class="min-w-200px">Nama Tanah</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($gedungs as $gedung )
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$gedung->nama_gedung}}</td>
            <td>{{$gedung->id_gedung}}</td>
            {{-- <td>{{$gedung->tanah->name}}</td> --}}
            <td>{{ $gedung->tanah->name }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<style>
    body {
        font-family: Arial, sans-serif;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
        text-align: center;
    }

    .header {
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 20px;
    }
</style>
