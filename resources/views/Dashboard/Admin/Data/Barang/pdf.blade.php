<div class="header">
    <p>Header Dokumen Resmi</p>
</div>

<table>
    <thead>
        <tr>
            <th class="w-10px pe-2">#</th>
            <th class="min-w-200px">ID Barang</th>
            <th class="min-w-125px">Nama Barang</th>
            <th class="min-w-150px">Nomor Ruangan</th>
            <th class="min-w-200px">Jumlah</th>
            <th class="min-w-125px">Status Barang</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($barangs as $barang )
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$barang->id_barang}}</td>
            <td>{{$barang->nama_barang}}</td>
            <td>{{$barang->ruangan_id}}</td>
            <td>{{$barang->jmlh_barang}}</td>
            <td>{{$barang->kondisi_barang}}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<style>
    body {
        font-family: Arial, sans-serif;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
        text-align: center;
    }

    .header {
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 20px;
    }
</style>
