<div class="header">
    <p>Header Dokumen Resmi</p>
</div>

<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Ruangan</th>
            <th>ID Ruangan</th>
            <th>No Ruangan</th>
            <th>PJ Ruangan</th>
            <th>Rayon</th>
            <th>Nama Gedung</th>
        </tr>
    </thead>
    <tbody>
        @foreach($ruangans as $ruangan)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $ruangan->nama_ruangan }}</td>
                <td>{{ $ruangan->id_ruangan }}</td>
                <td>{{ $ruangan->no_ruangan }}</td>
                <td>{{ $ruangan->pj_ruangan }}</td>
                <td>{{ $ruangan->rayon }}</td>
                <td>{{ $ruangan->gedung->nama_gedung }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

<style>
    body {
        font-family: Arial, sans-serif;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
        text-align: center;
    }

    .header {
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 20px;
    }
</style>
