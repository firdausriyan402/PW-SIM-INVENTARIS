@extends('layout.main')
@section('content')

<div class="card mb-10 border border-primary">
    <div class="card-header">
        <div class="card-title">
            <h2>{{$title}}</h2>
        </div>
        <div class="card-toolbar">
            <a href="{{route('ruangan.index')}}" class="btn btn-danger btn-sm">Back</a>
        </div>
    </div>
</div>
<div class="card rounded border border-primary">
    <div class="card-body">
        <form action="{{ route('ruangan.update', ['id' => $ruangan->id]) }}" method="POST">
            @csrf
            @method('PUT')        

            <!-- Nama Ruangan -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Nama Ruangan</span>
                        </label>
                        <input type="text" class="form-control form-control-solid"
                            placeholder="Masukan Nama Ruangan" name="nama_ruangan" value="{{ old('nama_ruangan', $ruangan->nama_ruangan) }}" />
                    </div>
                </div>

                <!-- Pilih Gedung -->
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                            <span class="required">Pilih Gedung </span>
                        </label>
                        <select name="gedung_id" data-control="select2"
                            data-placeholder="Pilih nama gedung"
                            class="form-select form-select-solid">
                            @foreach($gedungs as $gedung)
                                <option value="{{ $gedung->id }}" {{ old('gedung_id', $ruangan->gedung_id) == $gedung->id ? 'selected' : '' }}>
                                    {{ $gedung->nama_gedung }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <!-- Nomor Ruangan dan Pj Ruangan -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Nomor Ruangan</span>
                        </label>
                        <input type="text" class="form-control form-control-solid"
                            placeholder="Masukan Nomor Ruangan" name="no_ruangan" value="{{ old('no_ruangan', $ruangan->no_ruangan) }}"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                            <span class="required">Pj Ruangan </span>
                        </label>
                        <select name="pj_ruangan" data-control="select2"
                            data-placeholder="Pilih Pj Ruangan"
                            class="form-select form-select-solid">
                            <option value="">Pilih Pj Ruangan....</option>
                            <option value="Baik" {{ old('pj_ruangan', $ruangan->pj_ruangan) == 'Baik' ? 'selected' : '' }}>Baik</option>
                            <option value="Buruk" {{ old('pj_ruangan', $ruangan->pj_ruangan) == 'Buruk' ? 'selected' : '' }}>Buruk</option>
                        </select>
                    </div>
                </div>
            </div>

            <!-- Luas, Lebar, Panjang, dan Pilih Rayon -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="d-flex flex-column mb-8 fv-row">
                                <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                    <span class="required">Luas</span>
                                </label>
                                <input type="text" class="form-control form-control-solid" name="luas" placeholder="Masukan Luas ruangan" value="{{ old('luas', $ruangan->luas) }}" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="d-flex flex-column mb-8 fv-row">
                                <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                    <span class="required">Lebar</span>
                                </label>
                                <input type="text" class="form-control form-control-solid" name="lebar" placeholder="Masukan Lebar ruangan" value="{{ old('lebar', $ruangan->lebar) }}" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="d-flex flex-column mb-8 fv-row">
                                <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                    <span class="required">Panjang</span>
                                </label>
                                <input type="text" class="form-control form-control-solid" name="panjang" placeholder="Masukan Panjang ruangana" value="{{ old('panjang', $ruangan->panjang) }}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                            <span class="required">Pilih Rayon </span>
                        </label>
                        <select name="rayon" data-control="select2"
                            data-placeholder="Pilih Rayon"
                            class="form-select form-select-solid">
                            <option value="">Pilih Rayon....</option>
                            <option value="Baik" {{ old('rayon', $ruangan->rayon) == 'Baik' ? 'selected' : '' }}>Baik</option>
                            <option value="Buruk" {{ old('rayon', $ruangan->rayon) == 'Buruk' ? 'selected' : '' }}>Buruk</option>
                        </select>
                    </div>
                </div>
            </div>

            <!-- Tombol Reset dan Submit -->
            <div class="text-end">
                <button type="reset" class="btn btn-danger me-3 btn-sm">
                    <i class="ki-duotone ki-arrows-circle                        ">
                    <span class="path1"></span>
                    <span class="path2"></span>
                    </i>Reset</button>       
                <button type="submit" class="btn btn-primary btn-sm">
                    <span class="indicator-label">
                        <i class="bi bi-check-lg"></i>Submit
                    </span>
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
