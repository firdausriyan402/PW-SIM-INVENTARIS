@extends('layout.main')
@section('content')

<div class="card  mb-10 border border-primary">
    <div class="card-header">
        <div class="card-title">
            <h2>{{$title}}</h2>
        </div>
        <div class="card-toolbar">
            <a href="{{route('ruangan.index')}}" class="btn btn-danger btn-sm">Back</a>
        </div>
    </div>
</div>
<div class="card rounded border border-primary">
    <!--begin::Modal header-->
    <div class="card-body">
        <!--begin:Form-->
        <form class="form" action="{{route('ruangan.store')}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Name Ruangan</span>
                        </label>
                        <input type="text" class="form-control form-control-solid"
                            placeholder="Masukan Nama Ruangan" name="nama_ruangan" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <!--begin::Label-->
                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                            <span class="required">Pilih Gedung </span>
                        </label>
                        <!--end::Label-->
                        <!--begin::Select-->
                        <select name="gedung_id" data-control="select2"
                            data-placeholder="Pilih nama gedung"
                            class="form-select form-select-solid">
                            @foreach ( $gedungs as $gedung)
                                    @if (old('gedung_id') == $gedung->id)
                                        <option value="{{ $gedung->id }}" selected>{{ $gedung->nama_gedung }}</option>
                                     @else
                                         <option value="{{ $gedung->id }}">{{ $gedung->nama_gedung }}</option>
                                    @endif
                                @endforeach
                        </select>
                        <!--end::Select-->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-8 fv-row">
                        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                            <span class="required">Nomor Ruangan</span>
                        </label>
                        <input type="text" class="form-control form-control-solid"
                            placeholder="Masukan Nomor Ruangan" name="no_ruangan" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <!--begin::Label-->
                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                            <span class="required">Pj Ruangan </span>
                        </label>
                        <!--end::Label-->
                        <!--begin::Select-->
                        <select name="pj_ruangan" data-control="select2"
                            data-placeholder="Pilih Pj Ruangan"
                            class="form-select form-select-solid">
                            <option value="">Pilih Pj Ruangan....</option>
                            <option value="Baik">Baik</option>
                            <option value="Buruk">Buruk</option>
                        </select>
                        <!--end::Select-->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="d-flex flex-column mb-8 fv-row">
                                <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                    <span class="required">Luas</span>
                                </label>
                                <input type="text" class="form-control form-control-solid" name="luas" placeholder="Masukan Luas ruangan" />
                            </div>
    
                        </div>
                        <div class="col-lg-4">
                            <div class="d-flex flex-column mb-8 fv-row">
                                <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                    <span class="required">Lebar</span>
                                </label>
                                <input type="text" class="form-control form-control-solid" name="lebar" placeholder="masukan lebar ruangan" />
                            </div>
    
                        </div>
                        <div class="col-lg-4">
                            <div class="d-flex flex-column mb-8 fv-row">
                                <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                                    <span class="required">Panjang</span>
                                </label>
                                <input type="text" class="form-control form-control-solid" name="panjang" placeholder="masukan panjang ruangana" />
                            </div>
    
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="d-flex flex-column mb-5 fv-row">
                        <!--begin::Label-->
                        <label class="d-flex align-items-center fs-5 fw-bold mb-2">
                            <span class="required">Pilih Rayon </span>
                        </label>
                        <!--end::Label-->
                        <!--begin::Select-->
                        <select name="rayon" data-control="select2"
                            data-placeholder="Pilih Rayon"
                            class="form-select form-select-solid">
                            <option value="">Pilih Rayon....</option>
                            
                            @include('layout.include.rayon')
                        </select>
                        <!--end::Select-->
                    </div>
                </div>
            </div>
            <!--begin::Actions-->
            <div class="text-end">
                <button type="reset" class="btn btn-danger me-3 btn-sm">
                    <i class="ki-duotone ki-arrows-circle                        ">
                    <span class="path1"></span>
                    <span class="path2"></span>
                    </i>Reset</button>
                <button type="submit"
                    class="btn btn-primary btn-sm">
                    <span class="indicator-label"><i
                            class="bi bi-check-lg"></i>Submit</span>
                </button>
            </div>
            <!--end::Actions-->
        </form>
        <!--end:Form-->
    </div>
    <!--end::Modal body-->
</div>
@endsection