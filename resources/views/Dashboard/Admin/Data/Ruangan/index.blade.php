@extends('layout.main')
@section('content')
@if (session('berhasil'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session ('berhasil') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif 
<div class="card card-flush border border-primary mb-5 mt-xl-9">
    <div class="card-header">
        <div class="card-title">
            <h3 class="fw-bolder mb-1">{{$title}}</h3>
        </div>
        <div class="card-toolbar">
            <div class="d-flex flex-warp">
            <a href="{{route('ruangan.export')}}" class="btn btn-primary btn-sm me-2">Export Excel</a>
            <a href="{{route('ruangan.pdf')}}" class="btn btn-primary btn-sm me-2">Download PDF</button>
            <a href="{{route('ruangan.add')}}" class="btn btn-primary btn-sm me-2"><i class="fa fa-plus-circle"></i>Tambah
                    Data</a>
            </div>
        </div>
    </div>
</div>

<div class="card border border-success rounded mb-5">
    <!--begin::Card header-->
    <div class="card-header border-0 pt-6 bg-success">
        <!--begin::Card title-->
        <div class="card-title">
            <div class="d-flex align-items-center position-relative my-1">
                <span class="svg-icon svg-icon-1 position-absolute ms-6">
                    <i class="ki-duotone ki-magnifier                        ">
                        <span class="path1"></span>
                        <span class="path2"></span>
                       </i>
                </span>
                <!--end::Svg Icon-->
                <input type="text" data-kt-data-table-filter="search"
                    class="form-control form-control-solid w-250px ps-14"
                    placeholder="Search" />
            </div>
        </div>
        <div class="card-toolbar">
            <div class="d-flex justify-content-end" data-kt-data-table-toolbar="base">
                <!--begin::Filter-->
                <button type="button" class="btn btn-primary me-3 btn-sm"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                    <span class="svg-icon svg-icon-2">
                        <span class="svg-icon svg-icon-2">
                            <i class="ki-duotone ki-filter                        ">
                                <span class="path1"></span>
                                <span class="path2"></span>
                                </i>
                        </span>
                    </span>
                    <!--end::Svg Icon-->Filter</button>
                <!--begin::Menu 1-->
                <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px"
                    data-kt-menu="true">
                    <!--begin::Header-->
                    <div class="px-7 py-5">
                        <div class="fs-5 text-dark fw-bolder">Filter Options</div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Separator-->
                    <div class="separator border-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Content-->
                    <div class="px-7 py-5" data-kt-data-table-filter="form">
                        <!--begin::Input group-->
                        <div class="mb-10">
                            <label class="form-label fs-6 fw-bold">Status Permohonan :
                            </label>
                            <select class="form-select form-select-solid fw-bolder"
                                data-kt-select2="true" data-placeholder="---Pilih---"
                                data-allow-clear="true" data-kt-data-table-filter="role"
                                data-hide-search="true">
                                <option></option>
                                <option value="Skema PPKA v2">Skema PPKA v2</option>
                                <option value="Skema Percobaan">Skema Percoaan</option>
                                <option value="Skema Dua Percobaa">Skema Dua Percobaan
                                </option>
                                <option value="Skema RPK">Skema RPK</option>
                            </select>
                        </div>
                        <!--end::Input group-->
                        <!--begin::Actions-->
                        <div class="d-flex justify-content-end">
                            <button type="reset"
                                class="btn btn-light btn-active-light-primary fw-bold me-2 px-6"
                                data-kt-menu-dismiss="true"
                                data-kt-data-table-filter="reset">Reset</button>
                            <button type="submit" class="btn btn-primary fw-bold px-6"
                                data-kt-menu-dismiss="true"
                                data-kt-data-table-filter="filter">Apply</button>
                        </div>
                        <!--end::Actions-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Menu 1-->
            </div>
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-0">
        <!--begin::Table-->
        <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_datas_table">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">#</th>
                    <th class="min-w-200px">Nama Ruangan</th>
                    <th class="min-w-125px">ID Ruangan</th>
                    <th class="min-w-150px">Nomor Ruangan</th>
                    <th class="min-w-200px">Pj Ruangan</th>
                    <th class="min-w-125px">Rayon</th>
                    <th class="min-w-200px">Gedung</th>
                    <th class="text-start min-w-200px">Aksi</th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <tbody class="text-gray-600 fw-bold">
                @foreach ($ruangans as $ruangan )
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$ruangan->nama_ruangan}}</td>
                    <td>{{$ruangan->id_ruangan}}</td>
                    <td>{{$ruangan->no_ruangan}}</td>
                    <td>{{$ruangan->pj_ruangan}}</td>
                    <td>{{$ruangan->rayon}}</td>
                    <td>{{$ruangan->gedung->nama_gedung}}</td>
                    <td class="d-flex text-start">
                        <a href="{{ route('ruangan.edit', ['id' => $ruangan->id]) }}" class="btn btn-warning btn-sm me-2">
                            <i class="fa fa-edit"></i>
                            <span class="d-none d-lg-inline">Update</span>
                        </a>
                        <form action="{{ route('ruangan.destroy', ['id' => $ruangan->id]) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm">
                                <i class="fa fa-trash-alt"></i>
                                <span class="d-none d-lg-inline">Delete</span>
                            </button>
                        </form>
                        {{-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal{{ $ruangan->id }}">
                            Delete
                        </button> --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
</div>
@endsection
<!-- Tombol Delete -->

<!-- Modal Konfirmasi Hapus -->
{{-- <div class="modal fade" id="deleteModal{{ $ruangan->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $ruangan->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel{{ $ruangan->id }}">Konfirmasi Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah Anda yakin ingin menghapus data ini?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <form action="{{ route('ruangan.destroy', ['id' => $ruangan->id]) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div> --}}
