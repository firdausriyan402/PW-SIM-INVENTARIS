@extends('layout.main')
@section('content')
<div class="card card-flush border border-primary mb-5 mt-xl-9">
    <div class="card-header">
        <div class="card-title">
            <h3 class="fw-bolder mb-1">{{$title}}</h3>
        </div>
        <div class="card-toolbar">
            <div class="d-flex flex-warp">
            <a href="{{route('tanah.export')}}" class="btn btn-primary btn-sm me-2">Export Excel</a>
            <a href="{{route('tanah.pdf')}}" class="btn btn-primary btn-sm me-2">Download PDF</button>
            <a href="{{route('tanah.add')}}" class="btn btn-primary btn-sm me-2"><i class="fa fa-plus-circle"></i>Tambah
                    Data</a>
            </div>
        </div>
    </div>
</div>
<div class="card border border-success rounded mb-5">
    <!--begin::Card header-->
    <div class="card-header border-0 pt-6 bg-success">
        <!--begin::Card title-->
        <div class="card-title">
            <div class="d-flex align-items-center position-relative my-1">
                <span class="svg-icon svg-icon-1 position-absolute ms-6">
                    <i class="ki-duotone ki-magnifier                        ">
                        <span class="path1"></span>
                        <span class="path2"></span>
                       </i>
                </span>
                <!--end::Svg Icon-->
                <input type="text" data-kt-data-table-filter="search"
                    class="form-control form-control-solid w-250px ps-14"
                    placeholder="Search" />
            </div>
        </div>
        <div class="card-toolbar">
            <div class="d-flex justify-content-end" data-kt-data-table-toolbar="base">
                <!--begin::Filter-->
                <button type="button" class="btn btn-primary me-3 btn-sm"
                    data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen031.svg-->
                    <span class="svg-icon svg-icon-2">
                        <span class="svg-icon svg-icon-2">
                            <i class="ki-duotone ki-filter                        ">
                                <span class="path1"></span>
                                <span class="path2"></span>
                                </i>
                        </span>
                    </span>
                    <!--end::Svg Icon-->Filter</button>
                <!--begin::Menu 1-->
                <div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px"
                    data-kt-menu="true">
                    <!--begin::Header-->
                    <div class="px-7 py-5">
                        <div class="fs-5 text-dark fw-bolder">Filter Options</div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Separator-->
                    <div class="separator border-gray-200"></div>
                    <!--end::Separator-->
                    <!--begin::Content-->
                    <div class="px-7 py-5" data-kt-data-table-filter="form">
                        <!--begin::Input group-->
                        <div class="mb-10">
                            <label class="form-label fs-6 fw-bold">Status Permohonan :
                            </label>
                            <select class="form-select form-select-solid fw-bolder"
                                data-kt-select2="true" data-placeholder="---Pilih---"
                                data-allow-clear="true" data-kt-data-table-filter="role"
                                data-hide-search="true">
                                <option></option>
                                <option value="Skema PPKA v2">Skema PPKA v2</option>
                                <option value="Skema Percobaan">Skema Percoaan</option>
                                <option value="Skema Dua Percobaa">Skema Dua Percobaan
                                </option>
                                <option value="Skema RPK">Skema RPK</option>
                            </select>
                        </div>
                        <!--end::Input group-->
                        <!--begin::Actions-->
                        <div class="d-flex justify-content-end">
                            <button type="reset"
                                class="btn btn-light btn-active-light-primary fw-bold me-2 px-6"
                                data-kt-menu-dismiss="true"
                                data-kt-data-table-filter="reset">Reset</button>
                            <button type="submit" class="btn btn-primary fw-bold px-6"
                                data-kt-menu-dismiss="true"
                                data-kt-data-table-filter="filter">Apply</button>
                        </div>
                        <!--end::Actions-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Menu 1-->
            </div>
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-0">
        <!--begin::Table-->
        <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_datas_table">
            <!--begin::Table head-->
            <thead>
                <!--begin::Table row-->
                <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                    <th class="w-10px pe-2">#</th>
                    <th class="min-w-200px">Nama Tanah</th>
                    <th class="min-w-200px">ID Tanah</th>
                    <th class="min-w-150px">Lebar</th>
                    <th class="min-w-200px">panjang</th>
                    <th class="min-w-125px">Luas</th>
                    <th class="text-start min-w-200px">Aksi</th>
                </tr>
                <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <tbody class="text-gray-600 fw-bold">
                @foreach ($tanahs as $tanah)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$tanah->name}}</td>
                    <td>{{$tanah->id_tanah}}</td>
                    <td>{{$tanah->lebar}}</td>
                    <td>{{$tanah->panjang}}</td>
                    <td>{{$tanah->luas}}</td>
                    <td class="d-flex text-start">
                        <a href="{{ route('tanah.edit', ['id' => $tanah->id]) }}" class="btn btn-warning btn-sm me-2">
                            <i class="fa fa-edit"></i>
                            <span class="d-none d-lg-inline">Update</span>
                        </a>
                        <form action="{{ route('tanah.destroy', ['id' => $tanah->id]) }}" method="POST" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm">
                                <i class="fa fa-trash-alt"></i>
                                <span class="d-none d-lg-inline">Delete</span>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
</div>
@endsection