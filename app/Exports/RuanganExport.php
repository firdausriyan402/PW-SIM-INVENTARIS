<?php

namespace App\Exports;

use App\Models\Ruangan;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RuanganExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Ruangan::all();
    }

    public function map($ruangan): array
    {
        return [
            $ruangan->id,
            $ruangan->nama_ruangan,
            $ruangan->id_ruangan,
            $ruangan->gedung->nama_gedung,
            $ruangan->no_ruangan,
            $ruangan->luas,
            $ruangan->lebar,
            $ruangan->panjang,
            $ruangan->pj_ruangan,
            $ruangan->rayon,
        ];
    }

    public function headings(): array
    {
        return [
            'No',
            'Nama Ruangan',
            'ID Ruangan',
            'Gedung',
            'Nomor Ruangan',
            'Luas',
            'Lebar',
            'Panjang',
            'PJ Ruangan',
            'Rayon',
        ];
    }
}
