<?php

namespace App\Exports;

use App\Models\Gedung;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class GedungExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */

    public function collection()
    {
        return Gedung::all();
    }
    
    public function map($gedung): array
    {
        return [
            $gedung->id,
            $gedung->nama_gedung,
            $gedung->id_gedung,
            $gedung->tanah->name,
        ];
    }
    
    public function headings(): array
    {
        return [
            'No',
            'Nama Gedung',
            'ID Gedung',
            'Nama Tanah',
        ];
    }
}
