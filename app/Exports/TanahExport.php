<?php

namespace App\Exports;

use App\Models\Tanah;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TanahExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tanah::all();
    }

    public function map($tanah): array
    {
        return [
            $tanah->id,
            $tanah->name,
            $tanah->id_tanah,
            $tanah->panjang,
            $tanah->luas,
            $tanah->lebar,
        ];
    }

    public function headings(): array
    {
        return [
            'No',
            'Nama Tanah',
            'ID Tanah',
            'Panjang',
            'Luas',
            'Lebar',
        ];
    }
}
