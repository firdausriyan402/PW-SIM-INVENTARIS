<?php

namespace App\Exports;

use App\Models\Barang;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BarangExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Barang::all();
    }

    public function map($barang): array
    {
        return [
            $barang->id,
            $barang->nama_barang,
            $barang->id_barang,
            $barang->ruangan_id,
            $barang->jmlh_barang,
            $barang->kondisi_barang,
        ];
    }

    public function headings(): array
    {
        return [
            'No',
            'Nama Barang',
            'ID Barang',
            'Ruangan ID',
            'Jumlah Barang',
            'Kondisi Barang',
        ];
    }
}
