<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class UserHelper
{
    public static function calculateProfileCompletion($user)
    {
        $filledFields = 0;

        // Add fields that contribute to profile completion
        $fieldsToCheck = ['first_name', 'last_name', 'alamat', 'no_hp', 'jabatan','name','guru_matapelajaran','password','email'];

        foreach ($fieldsToCheck as $field) {
            if (!empty($user->$field)) {
                $filledFields++;
            }
        }

        // Calculate completion percentage
        $completionPercentage = ($filledFields / count($fieldsToCheck)) * 100;

        return round($completionPercentage);
    }
}
