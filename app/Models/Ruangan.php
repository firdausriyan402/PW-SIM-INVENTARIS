<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    // protected $fillable = ['id_gedung', 'nama_gedung', 'id_gedung'];

    
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->generateIdTanah();
        });
    }

    public function generateIdTanah()
    {
        $lastRecord = static::latest('id_ruangan')->first();

        if ($lastRecord) {
            $lastId = substr($lastRecord->id_ruangan, 3);
            $newId = str_pad($lastId + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $newId = '000001';
        }

        $this->id_ruangan = 'RGN' . $newId;
    }

    public function gedung()
    {
        return $this->belongsTo(Gedung::class, 'gedung_id');
    }

    public function barang()
    {
        return $this->hasMany(Barang::class, 'ruangan_id');
    }

}
