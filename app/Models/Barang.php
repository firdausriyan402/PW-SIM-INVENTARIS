<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->generateIdBarang();
        });
    }

    public function generateIdBarang()
    {
        $lastRecord = static::latest('id_barang')->first();

        if ($lastRecord) {
            $lastId = substr($lastRecord->id_barang, 4);
            $newId = str_pad($lastId + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $newId = '000001';
        }

        $this->id_barang = 'BRNG' . $newId;
    }

    public function ruangans()
    {
        return $this->hasMany(Ruangan::class,'ruangan_id');
    }



}
