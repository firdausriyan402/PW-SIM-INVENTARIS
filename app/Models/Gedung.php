<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gedung extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    // protected $fillable = ['id_gedung', 'nama_gedung', 'id_gedung'];

    
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->generateIdGedung();
        });
    }

    public function generateIdGedung()
    {
        $lastRecord = static::latest('id_gedung')->first();
    
        if ($lastRecord) {
            $lastId = (int) substr($lastRecord->id_gedung, 4); // Assuming 'GDNG' has 4 characters
            $newId = str_pad($lastId + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $newId = '000001';
        }
    
        $this->id_gedung = 'GDNG' . $newId;
    }

    public function tanah()
    {
        return $this->belongsTo(Tanah::class, 'tanah_id');
    }

    public function ruangans()
    {
        return $this->hasMany(Ruangan::class, 'gedung_id');
    }

}
