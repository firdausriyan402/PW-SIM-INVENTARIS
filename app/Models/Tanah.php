<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tanah extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'panjang', 'luas', 'lebar', 'id_tanah'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->generateIdTanah();
        });
    }

    public function generateIdTanah()
    {
        $lastRecord = static::latest('id_tanah')->first();

        if ($lastRecord) {
            $lastId = substr($lastRecord->id_tanah, 3);
            $newId = str_pad($lastId + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $newId = '000001';
        }

        $this->id_tanah = 'TNH' . $newId;
    }

    public function gedung()
    {
        return $this->hasMany(Gedung::class);
    }
}
