<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    // Route Login
    public function index()
    {
        return view('auth.login');
    }

    public function authanticate(Request $request)
    {
        // validation
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if ($user->role == 'Admin') {
                return redirect()->route('admin.dashboard');
            } elseif ($user->role == 'User') {
                return redirect()->route('admin.dashboard');
            } else {
                return redirect()->route('admin.dashboard');
            };
        }

        return redirect()->back()->with('error', 'Login failed, please try again!');
    }

    // Route register
    public function register()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:50',
            // 'username' => ['required','min:3','max:50', 'unique:users'],
            'role' => 'required',
            'password' => 'required|min:8|max:50',
            'email' => 'required|email|unique:users,email',
        ]);

        $validateData['password'] = bcrypt($validateData['password']);

        User::create($validateData);

        return redirect('/')->with('success', 'Registration successful! Please log in.');
    }

    // Route Reset Pw
    public function reset()
    {
        return view('auth.reset');
    }

    public function logout()
    {
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }
}
