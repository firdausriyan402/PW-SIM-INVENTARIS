<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PenggunaController extends Controller
{
    //
    public function index() {
        $penggunas = User::all();
        $title = 'Data Pengguna';
        return view('Dashboard.Admin.Pengguna.index',compact('title','penggunas'));
    }

    
    public function create() {
        $title = 'Tambah Data Pengguna';
        return view('Dashboard.Admin.Pengguna.add',compact('title'));
    }

    public function edit() {
        $title = 'edit Data Pengguna';
        return view('Dashboard.Admin.Pengguna.edit',compact('title'));
    }

    public function myProfile() {
        $user = auth()->user(); // Mengambil pengguna yang saat ini login
        $title = 'My Profile';
        return view('Dashboard.Admin.Pengguna.myProfile', compact('title', 'user'));
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'nullable',
            'first_name' => 'nullable',
            'last_name' => 'nullable',
            'alamat' => 'nullable',
            'guru_matapelajaran' => 'nullable',
            'no_hp' => 'nullable',
            'jabatan' => 'nullable',
        ]);
    
        $user = Auth::user();
        $user->update(array_filter($validatedData)); // Use array_filter to remove null values from the array
    
        return redirect()->route('pengguna.myProfile')->with('success', 'Profile updated successfully!');
    }
    
}
