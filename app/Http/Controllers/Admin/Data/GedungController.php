<?php

namespace App\Http\Controllers\Admin\Data;

use App\Exports\GedungExport;
use App\Http\Controllers\Controller;
use App\Models\Gedung;
use App\Models\Tanah;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PDF;

class GedungController extends Controller
{
    //
    public function index() {

        $gedungs = Gedung::all();
        $title = 'Data Gedung';
        return view('Dashboard.Admin.Data.Gedung.index',compact('title','gedungs'));
    }

    public function create() {
        $tanahs = Tanah::all();

        $title = 'Tambah Data Gedung';
        return view('Dashboard.Admin.Data.Gedung.add',compact('title','tanahs'));
    }

    public function store(Request $request) {
        
        $validateDate = $request->validate([
            'nama_gedung' => 'required|max:255',
            'tanah_id' => 'required',
        ]);

        Gedung::create($validateDate);

        return redirect()->route('gedung.index')->with('berhasil','good job!');

    }

    public function edit($id) {

        $gedung = Gedung::findOrFail($id);
        $tanahs = Tanah::all();
        $title = 'Edit Data Gedung';
        return view('Dashboard.Admin.Data.Gedung.edit',compact('title','gedung','tanahs'));
    }

    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'nama_gedung' => 'required|max:255',
            'tanah_id' => 'required',
        ]);

        $gedungs = Gedung::findOrFail($id);
        $gedungs->update($validatedData);

        return redirect()->route('gedung.index')->with('berhasil', 'Data berhasil diupdate');
    }

    public function destroy($id)
    {
        $gedungs = Gedung::findOrFail($id);
        $gedungs->delete();

        return redirect()->route('gedung.index')->with('berhasil', 'Data Berhasil Di HApus');
    }



    public function pdf()
    {
        $gedungs = Gedung::all();
        $pdf = PDF::loadView('Dashboard.Admin.Data.Gedung.pdf', compact('gedungs'));
    
        // Download PDF
        return $pdf->download('Gedung.pdf');
    }
    

    public function export()
    {
        return (new GedungExport)->download('gedung-'.Carbon::now()->timestamp.'.xlsx');
    }
}

