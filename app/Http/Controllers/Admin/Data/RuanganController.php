<?php

namespace App\Http\Controllers\Admin\Data;

use App\Exports\RuanganExport;
use App\Http\Controllers\Controller;
use App\Models\Gedung;
use App\Models\Ruangan;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PDF;

class RuanganController extends Controller
{
    //
    public function index()
    {

        $ruangans = Ruangan::all();
        $title = 'Data Ruangan';
        return view('Dashboard.Admin.Data.Ruangan.index', compact('title', 'ruangans'));
    }

    public function create()
    {

        $gedungs = Gedung::all();
        $title = 'Tambah Data Ruangan';
        return view('Dashboard.Admin.Data.Ruangan.add', compact('title', 'gedungs'));
    }

    public function store(Request $request)
    {
        $validateDate = $request->validate([
            'nama_ruangan' => 'required',
            'no_ruangan' => 'required',
            'rayon' => 'required',
            'gedung_id' => 'required',
            'luas' => 'required',
            'lebar' => 'required',
            'panjang' => 'required',
            'pj_ruangan' => 'required',
        ]);

        Ruangan::create($validateDate);

        return redirect()->route('ruangan.index')->with('berhasil', 'goos');
    }

    public function edit($id)
    {
        $ruangan = Ruangan::findOrFail($id);
        $gedungs = Gedung::all();
        $title = 'Edit Data Ruangan';
        return view('Dashboard.Admin.Data.Ruangan.edit', compact('title', 'ruangan', 'gedungs'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama_ruangan' => 'required',
            'no_ruangan' => 'required',
            'rayon' => 'required',
            'gedung_id' => 'required',
            'luas' => 'required',
            'lebar' => 'required',
            'panjang' => 'required',
            'pj_ruangan' => 'required',
        ]);

        $ruangan = Ruangan::findOrFail($id);
        $ruangan->update($validatedData);

        return redirect()->route('ruangan.index')->with('berhasil', 'Data berhasil diupdate');
    }

    public function pdf()
    {
        $ruangans = Ruangan::all();
        $pdf = PDF::loadView('Dashboard.Admin.Data.Ruangan.pdf', compact('ruangans'));
    
        // Download PDF
        return $pdf->download('Ruangan.pdf');
    }

    public function destroy($id)
    {
        $ruangan = Ruangan::findOrFail($id);
        $ruangan->delete();

        return redirect()->route('ruangan.index')->with('berhasil', 'Data berhasil dihapus');
    }

    public function export()
    {
        return (new RuanganExport)->download('ruangan-'.Carbon::now()->timestamp.'.xlsx');
    }

}
