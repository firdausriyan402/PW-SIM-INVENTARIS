<?php

namespace App\Http\Controllers\Admin\Data;

use App\Exports\TanahExport;
use App\Http\Controllers\Controller;
use App\Models\Tanah;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
// use Barryvdh\DomPDF\PDF;
use PDF;

class TanahController extends Controller
{
    //
    public function index() {

        $tanahs = Tanah::all();
        $title = 'Data Tanah';
        return view('Dashboard.Admin.Data.Tanah.index',compact('title','tanahs'));
    }

    public function create() {
        $title = 'Tambah Data Tanah';
        return view('Dashboard.Admin.Data.Tanah.add',compact('title'));
    }

    public function store(Request $request) {
        
        $request->validate([
            'name' => 'required',
            'panjang' => 'required',
            'luas' => 'required',
            'lebar' => 'required',
            // 'id_tanah' => 'required'
        ]);

        Tanah::create($request->all());

        return redirect()->route('tanah.index')
        ->with('berhasil', 'Data Berhasil di tambahkan');

    }

    public function edit($id) {
        $tanah = Tanah::findOrFail($id);
        $title = 'Edit Data Tanah';
        return view('Dashboard.Admin.Data.Tanah.edit',compact('title','tanah'));
    }

    public function update(Request $request, $id) {
        $validatedData = $request->validate([
            'name' => 'required',
            'panjang' => 'required',
            'luas' => 'required',
            'lebar' => 'required',
        ]);

        $tanahs = Tanah::findOrFail($id);
        $tanahs->update($validatedData);

        return redirect()->route('tanah.index')->with('berhasil', 'Data berhasil diupdate');
    }

    public function destroy($id)
    {
        $tanahs = Tanah::findOrFail($id);
        $tanahs->delete();

        return redirect()->route('tanah.index')->with('berhasil', 'Data Berhasil Di HApus');
    }




    public function pdf()
    {
        $tanahs = Tanah::all();
        $pdf = PDF::loadView('Dashboard.Admin.Data.Tanah.pdf', compact('tanahs'));
    
        // Download PDF
        return $pdf->download('Tanah.pdf');
    }

    public function export()
    {
        return (new TanahExport)->download('tanah-'.Carbon::now()->timestamp.'.xlsx');
    }
}
