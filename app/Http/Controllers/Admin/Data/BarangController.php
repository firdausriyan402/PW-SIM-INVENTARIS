<?php

namespace App\Http\Controllers\Admin\Data;

use App\Exports\BarangExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\Ruangan;
use Carbon\Carbon;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BarangController extends Controller
{
    //
    public function index() {

        $barangs = Barang::all();
        $title = 'Data Barang';
        return view('Dashboard.Admin.Data.Barang.index',compact('title','barangs'));
    }

    public function create() {
        $ruangans = Ruangan::all();

        $title = 'Tambah Data Barang';
        return view('Dashboard.Admin.Data.Barang.add',compact('title','ruangans'));
    }

    public function store(Request $request) {
        $tambahbarang = $request->validate([
            'nama_barang' => 'required',
            'jmlh_barang' => 'required',
            'ruangan_id' => 'required',
            'kondisi_barang' => 'required',
        ]);

        Barang::create($tambahbarang);

        return redirect()->route('barang.index')->with('berhasil','Goodjob');

    }

    public function edit($id) {
        $barang = Barang::findOrFail($id);
        $ruangans = Ruangan::all();
        $title = 'Edit Data Barang';
        return view('Dashboard.Admin.Data.Barang.edit',compact('title', 'barang', 'ruangans'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nama_barang' => 'required',
            'jmlh_barang' => 'required',
            'ruangan_id' => 'required',
            'kondisi_barang' => 'required',
        ]);

        $barangs = Barang::findOrFail($id);
        $barangs->update($validatedData);

        return redirect()->route('barang.index')->with('berhasil', 'Data berhasil diupdate');
    }

    public function destroy($id)
    {
        $barangs = Barang::findOrFail($id);
        $barangs->delete();

        return redirect()->route('barang.index')->with('berhasil', 'Data berhasil dihapus');
    }

    public function pdf()
    {
        $barangs = Barang::all();
        $pdf = PDF::loadView('Dashboard.Admin.Data.Barang.pdf', compact('barangs'));
    
        // Download PDF
        return $pdf->download('Barang.pdf');
    }


    public function export()
    {
        return (new BarangExport)->download('barang-'.Carbon::now()->timestamp.'.xlsx');
    }
}
