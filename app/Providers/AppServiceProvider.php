<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
        // $this->registerPolicies();

        Gate::define('admin-access', function ($user) {
            return $user->role === 'Admin';
        });
    
        Gate::define('user-access', function ($user) {
            return $user->role === 'User';
        });
    
        Gate::define('guru-access', function ($user) {
            return $user->role === 'Guru';
        });
    }
}
